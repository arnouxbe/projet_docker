# Projet docker

**Emmanuel Arnoux**

**Lilian Steimer**

----

Ce projet est réalisé dans le cadre de l'enseignement **ASSR-MI4 : Architecture DevOps**.

Le but de ce projet et de mettre en place un déploiement complet de **Leed RSS** avec **Docker**.

> Lien du sujet : 
> https://beniamid.gricad-pages.univ-grenoble-alpes.fr/mi4_architecture_dev_ops/projet.pdf

## Installation
----

Assurez-vous d'avoir installé **Git** et **Docker-compose** sur votre ordinateur.

Clonez le dépôt Git contenant le projet à l'aide de la commande suivante :

```shell
$ git clone https://github.com/nom-du-dépôt.git
```

Accédez au répertoire du projet avec la commande :

```shell
$ cd nom-du-dépôt
```

Exécutez la commande suivante pour construire et démarrer les conteneurs Docker avec `docker-compose` :

```
$ docker-compose up
```

Cela construira les images Docker définies dans le fichier docker-compose.yml et démarrera les conteneurs associés. Vous pouvez maintenant accéder à l'application en utilisant l'adresse IP et le port définis dans le fichier docker-compose.yml.

Notez que la commande `docker-compose up` démarre les conteneurs en mode interactif. 

Si vous souhaitez les démarrer en arrière-plan, utilisez la commande `docker-compose up --detach` .


## Utilisation
----

Dans un navigateur, si installé en local ce rendre sur `http://localhost:<HOST_PORT>/leed/install.php` (HOST_PORT definie dans le fichier `.env`)

- Les variables entre crochets <> sont definit dans le `.env `
- Remplir les champs suivant comme tel ;

## Base de données

    Hôte : db
    Identifiant : <MYSQL_USER>
    Mot de passe : <MYSQL_PASSWORD>
    Base : <MYSQL_DATABASE>

## Administrateur

    Identifiant : root
    Mot de passe : <MYSQL_ROOT_PASSWORD>


