FROM php:7.4-apache

ENV DEBIAN_FRONTEND=noninteractive

# Installation des paquets / dépendances
RUN apt-get update -y && apt-get install -y \
  unzip \
  wget \
  cron \
  libfreetype6-dev \
  libjpeg62-turbo-dev \
  libpng-dev

RUN docker-php-ext-install gd mysqli

# Téléchargement de Leed-RSS
RUN wget https://github.com/LeedRSS/Leed/archive/refs/heads/master.zip -O /tmp/master.zip
RUN unzip /tmp/master.zip -d /var/www/html

# On renomme le dossier de Leed-RSS
RUN mv /var/www/html/Leed-master /var/www/html/leed

# Changement des droits
RUN chmod -R 777 /var/www/html/leed

# Copie du script d'entrypoint
COPY ./Entrypoint /Entrypoint

# Rendre l'entrypoint exécutable
RUN chmod +x /Entrypoint

# Définision de l'entrypoint
ENTRYPOINT ["bash", "/Entrypoint"]